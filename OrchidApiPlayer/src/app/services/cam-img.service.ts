import { Injectable } from '@angular/core'
import { HttpClient, 
         HttpHeaders, 
         HttpErrorResponse } from '@angular/common/http'
import { throwError } from 'rxjs'
import { catchError } from "rxjs/operators"

@Injectable({
    providedIn: 'root'
})
export class CamImgService {

    private _oap = new OrchidApiParameters()
    
    set user_session_id(id:string){
        
        this._oap.user_session_id = id
    }
    get user_session_id(){

        return this._oap.user_session_id
    }
    
    private _is_user_session_id_ready:boolean = false
    get is_ready(): boolean {

        return this._is_user_session_id_ready;
    }

    private httpHeader = {

        headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    }
    
    inactive_streams_id:number[] = new Array()

    constructor(private http:HttpClient) {

        this.get_user_session_id()
    }

    private get_user_session_id(){

        this.api_call_get_user_session_id().subscribe( data => {

            this._oap.user_session_id = data.id
            this._is_user_session_id_ready = true
        })
    }

    private api_call_get_user_session_id(){

        return this.http
                .post<any>( this._oap.url_user_session, 
                            JSON.stringify(this._oap.session_input), 
                            this.httpHeader)

                .pipe(  catchError((error: HttpErrorResponse) => {
                        return throwError(
                            `Failed to retrieve user session id. Status: ${
                            error.statusText || "Unknown"} `
                )}))
    }

    public api_call_get_live_streams_names_n_ids(){

        return  this.http
                .get<any>(this._oap.url_list_of_live_streams())

                .pipe( catchError((error: HttpErrorResponse) => {
                        return throwError(
                            `Failed to retrieve live streams names and ids. Status: ${
                            error.statusText || "Unknown"} `
                )}))
    }

    public api_call_get_streams_imgs(stid:number){

        return  this.http
                .get(   this._oap.url_streams_imgs(stid), {
                        headers: {'Content-Type': 'image/jpg'},
                        responseType: 'blob' })

                .pipe(  catchError((error: HttpErrorResponse) => {
                        this.inactive_streams_id.push(stid)
                        return throwError(
                            `Failed to fetch the stream image id ${stid}. Status: ${
                            error.statusText || "Unknown"} `
                )}))
    }
}


class OrchidApiParameters {

    constructor(){}

    public url_base:string = "https://orchid.ipconfigure.com/service/"

    public url_user_session:string = this.url_base + "sessions/user"
    
    public user_session_id:string = ""

    public url_streams_imgs(stream_id:number):string {

        return  this.url_base + 
                "streams/" + 
                stream_id + 
                "/frame" + 
                "?sid=" + 
                this.user_session_id + 
                "&time=0"
    }

    public url_list_of_live_streams():string {

        return  this.url_base + 
                "streams" + 
                "?sid=" + 
                this.user_session_id + 
                "&live=true"
    }

    public session_input : Object = {

        "username" : "liveviewer",
        "password" : "tpain",
        "expiresIn": 50000,
        "cookie"   : "session"
    }
}

/* ---------------------------------------------------------

CamImgService is the data source for the application.
It's part of the "M" in the MVC architecture

It gets the data from the Orchid Core API by 
using the HttpClient to fetch the following:

- the user session id
- the streams names and ids
- the streams images

-----------------------------------------------------------------*/
