import { TestBed } from '@angular/core/testing'
import { CamImgService } from './cam-img.service'

import { HttpClientTestingModule, 
         HttpTestingController   } from '@angular/common/http/testing'

describe('CamImgService', () => {

    let service : CamImgService
    let httpTC  : HttpTestingController;
    let oapt    : OrchidApiParametersTest

    beforeEach(() => {

        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule]
        })

        service = TestBed.inject(CamImgService)
        httpTC  = TestBed.inject(HttpTestingController)
        oapt    = new OrchidApiParametersTest()

        const api_call = httpTC.expectOne({

            method: "POST",
            url: oapt.url_user_session,
        })
    })

    afterEach(() => {

        httpTC.verify()
    })

    it('should be creatable', () => {

        expect(service).toBeTruthy()
    })

    it('should send the correct request to fetch the user session id ', () => {

        service['api_call_get_user_session_id']().subscribe()

        const api_call = httpTC.expectOne({

            method: "POST",
            url: oapt.url_user_session,
        });

        expect(api_call.request.body).toEqual(JSON.stringify(oapt.session_input))
    })

    it('should send the correct request to fetch live streams names and ids ', () => {

        service.user_session_id = oapt.user_session_id

        service.api_call_get_live_streams_names_n_ids().subscribe()
        
        const api_call = httpTC.expectOne({

            method: "GET",
            url: oapt.url_list_of_live_streams()
        })

        expect(true).toBeTruthy()
    })

    it('should send the correct request to fetch streams images', () => {

        service.user_session_id = oapt.user_session_id

        service.api_call_get_streams_imgs(1234).subscribe()
        
        const api_call = httpTC.expectOne({
            
            method: "GET",
            url: oapt.url_streams_imgs(1234)
        })

        expect(true).toBeTruthy()
    })

})

class OrchidApiParametersTest {

    constructor(){}

    public url_base:string = "https://orchid.ipconfigure.com/service/"

    public url_user_session:string = this.url_base + "sessions/user"
    
    public user_session_id:string  = 'mock_id_1234'

    public url_streams_imgs(stream_id:number):string {

        return  this.url_base + 
                "streams/" + 
                stream_id + 
                "/frame" + 
                "?sid=" + 
                this.user_session_id + 
                "&time=0"
    }

    public url_list_of_live_streams():string {

        return  this.url_base + 
                "streams" + 
                "?sid=" + 
                this.user_session_id + 
                "&live=true"
    }

    public session_input : Object = {

        "username" : "liveviewer",
        "password" : "tpain",
        "expiresIn": 50000,
        "cookie"   : "session"
    }
}