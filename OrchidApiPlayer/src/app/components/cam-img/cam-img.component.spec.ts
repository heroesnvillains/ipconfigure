import { ComponentFixture, TestBed } from '@angular/core/testing'
import { By } from '@angular/platform-browser'
import { CamImgComponent } from './cam-img.component'

describe('CamImgComponent', () => {

    let component : CamImgComponent;
    let fixture   : ComponentFixture<CamImgComponent>;

    beforeEach( () => {

        TestBed.configureTestingModule({
            declarations: [ CamImgComponent ]
        })

        fixture   = TestBed.createComponent(CamImgComponent);
        component = fixture.componentInstance;
    })

    it('should be creatable', () => {

        expect(component).toBeTruthy();
    })

    it('should contain an <img> element for the stream image', () => {

        const img_elem = fixture.debugElement.queryAll(By.css('img'))
        expect(img_elem.length == 1).toBeTruthy();
    })

    it('should set the dimensions of the <img> element to 400 X 400 px', () => {

        const img_elem = fixture.debugElement.query(By.css('img')).nativeNode
        expect(img_elem.width == 400 && img_elem.height == 400).toBeTruthy();
    })

    it('should contain a <p> element for the stream name', () => {

        const p_elem = fixture.debugElement.queryAll(By.css('p'))
        expect(p_elem.length == 1).toBeTruthy();
    })

})
