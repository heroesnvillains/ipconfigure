import { Component, OnInit, Input } from '@angular/core'
import { DomSanitizer } from '@angular/platform-browser'

@Component({
  selector: 'app-cam-img',
  templateUrl: './cam-img.component.html',
  styleUrls: ['./cam-img.component.css']
})
export class CamImgComponent implements OnInit {

    @Input() stream_img  : any
    @Input() stream_name : any

    constructor(public sanitizer:DomSanitizer) {}

    ngOnInit(): void {}

}


/* ---------------------------------------------------------

CamImgComponent displays a camera stream name and image.

- a collection of these is used as children by the CamImgContainerComponent

- it's a dumb component that draws the camera stream image and 
  prints out the name passed in by the parent (the CamImgContainerComponent)

------------------------------------------------------------- */