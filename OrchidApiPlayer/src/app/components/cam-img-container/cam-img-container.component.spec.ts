import { ComponentFixture, TestBed } from '@angular/core/testing'
import { By } from '@angular/platform-browser'
import { Component, Input } from '@angular/core'
import { CamImgContainerComponent } from './cam-img-container.component'
import { CamImgService } from '../../services/cam-img.service'
import { of } from "rxjs"

describe('CamImgContainerComponent', () => {

    let component: CamImgContainerComponent;
    let fixture  : ComponentFixture<CamImgContainerComponent>;

    let mock_blob:Blob
    let mock_image_str:string

    const mockService = jasmine.createSpyObj([
        'api_call_get_live_streams_names_n_ids', 
        'api_call_get_streams_imgs'
    ])
	
    @Component({
        
        selector: 'app-cam-img',
        template: '<div></div>'
    })
    class CamImgComponentMock {

        @Input() stream_img  : any
        @Input() stream_name : any
    }

    const blob2string = (blob: Blob) => {

        return new Promise(resolve => {
            
            const reader     = new FileReader()
            reader.onloadend = () => resolve(reader.result)

            reader.readAsDataURL(blob)
        })
    }

    beforeEach(async () => {

        mock_image_str = "data:image/jpeg;base64,abeautifuljpegimage0"
        mock_blob      = await(await fetch(mock_image_str)).blob()
    })

    beforeEach(() => {
        
        TestBed.configureTestingModule({
            
            declarations: [ CamImgContainerComponent, CamImgComponentMock ],
            providers   : [
                { provide: CamImgService, useValue: mockService }
            ]
        })
        
        fixture   = TestBed.createComponent(CamImgContainerComponent)
        component = fixture.componentInstance
    })

    it('should be creatable', () => {

        expect(component).toBeTruthy();
    })

    it('should update the streams images every 5000 msecs', () => {

        expect(component.update_rate_msecs == 5000).toBeTruthy();
    })

    it('should redraw <app-cam-img> elements when streams_idx[] changes', () => {

        component.streams_idx.push(0)
        component.streams_idx.push(1)
        component.streams_idx.push(2)

        fixture.detectChanges()

        const child_nodes       = fixture.debugElement
                                  .queryAll(By.css('app-cam-img'))

        const child_nodes_name  = fixture.debugElement
                                  .query(By.css('app-cam-img')).name;
        
        expect(
            
            child_nodes_name  === 'app-cam-img' && 
            child_nodes.length == component.streams_idx.length

        ).toBeTruthy()
    })

    it('should correctly populate the streams names and ids', () => {
        
        const mock_list_of_streams = {

            streams : [{name: 'stream1', id: 111}, {name: 'stream2', id: 222}]
        }

        mockService.api_call_get_live_streams_names_n_ids
            .and
            .returnValue(of(mock_list_of_streams))

        mock_list_of_streams.streams
            .forEach(s => 
                
                mockService.api_call_get_streams_imgs
                    .withArgs(s.id)
                    .and
                    .returnValue(of(mock_blob))
            )
         
        component['get_active_streams']()
        component['inactive_streams_ids'] = [55,66]  
        component['populate_streams_arrays']()

        const cs_names = component.streams_names
        const cs_ids   = component['streams_ids']
        const mlss     = <Array<Object>> mock_list_of_streams.streams

        expect(

            mlss
            .map((s,i) => { 

                return  cs_names[i] === s['name'] && cs_ids[i] === s['id'].toString()
            })
            .reduce((boo1_all, boo1) => boo1_all && boo1 , true)

        ).toBeTruthy()
    })

    it('should correctly populate the streams image urls', async () => {
        
        mockService.api_call_get_streams_imgs
            .withArgs(1234)
            .and
            .returnValue(of(mock_blob))

        component['get_a_stream_img'](1234,0)

        const blob_url = component.streams_img_urls[0]
        const blob     = await fetch(blob_url).then(b => b.blob())
        const blob_str = await blob2string(blob)

        expect(mock_image_str).toEqual(<string>blob_str)
    })
})
