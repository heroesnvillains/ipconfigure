import { Component, OnInit } from '@angular/core';
import { CamImgService } from '../../services/cam-img.service'
import { timer } from 'rxjs'

@Component({

    selector: 'app-cam-img-container',
    templateUrl: './cam-img-container.component.html',
    styleUrls: ['./cam-img-container.component.css']
})
export class CamImgContainerComponent implements OnInit {

    streams_img_urls:string[] = new Array()
    streams_names:string[]    = new Array()
    streams_idx:number[]      = new Array()
    
    private streams_ids:string[] = new Array()
    
    private _update_rate_msecs:number = 5000
    get update_rate_msecs():number {

        return this._update_rate_msecs;
    }

    private _timer = timer(1000, this.update_rate_msecs);
    get timer() {

        return this._timer;
    }

    private streams_names_n_ids:any

    private inactive_streams_ids:any
    private more_inactive_streams_ids = [1989]

    private is_active_streams_data_ready = false
    private is_streams_arrays_initialized = false

    constructor(private cam_img_svc:CamImgService) { 

        this.timer.subscribe( count => {

            if(this.cam_img_svc.is_ready == false) return

            console.log('count: ' + count)

            if(!this.is_active_streams_data_ready)
                this.get_active_streams()

            if( this.is_active_streams_data_ready && 
                !this.is_streams_arrays_initialized ) {

                this.inactive_streams_ids = this.cam_img_svc.inactive_streams_id
                this.populate_streams_arrays()
            }
                
            if( this.is_active_streams_data_ready && 
                this.is_streams_arrays_initialized ) {
                
                this.get_streams_imgs()
            }
        })
    }

    ngOnInit(): void {}

    private populate_streams_arrays(){

        let idx = 0

        this.streams_names_n_ids.streams
            .filter((s:any) => {

                return !(this.inactive_streams_ids.some((id: any) => id == s.id) ||
                         this.more_inactive_streams_ids.some(id => id == s.id))
            })
            .forEach((s:any) => {
                
                this.streams_names.push(s.name)
                this.streams_ids.push(s.id.toString())
                this.streams_idx.push(idx)
                this.streams_img_urls.push('')

                idx++
            })

        this.is_streams_arrays_initialized = true
    }

    private get_active_streams(){

        this.cam_img_svc.api_call_get_live_streams_names_n_ids()
            .subscribe( data => {

                this.streams_names_n_ids = data

                this.streams_names_n_ids.streams
                    .forEach((s:any) => {

                        this.cam_img_svc.api_call_get_streams_imgs(s.id)
                            .subscribe( blob => {

                                this.is_active_streams_data_ready = true
                            })
                    })
            })
    }

    private get_streams_imgs(){

        this.streams_ids
            .forEach( (stid_str, idx) => {

                this.get_a_stream_img(parseInt(stid_str), idx)
            })
    }

    private get_a_stream_img(stid:number, idx:number){

        this.cam_img_svc.api_call_get_streams_imgs(stid)
            .subscribe( blob => {
            
                let blob_url = URL.createObjectURL(blob);     
                this.streams_img_urls[idx] = blob_url
            })
    }
}


/* ---------------------------------------------------------

CamImgContainerComponent is the container for the camera streams images

- it uses the Flex Box to show the camera stream images as a grid

- it gets the data (camera stream names and images) from the CamImgService

- its children are instances of CamImgComponents
  (a CamImgComponent displays a camera stream name and image)

- it feeds the camera streams names and images to the children


Member variables/fields:

The indexes of the following arrays correspond to the indexes of 
the children (the CamImgComponents)

- streams_idx is an array of the indexes of the CamImgComponents

    its values are are the same as its indexes: 
        0, 1, 2, ... the total number of CamImgComponents 

    Angular detects changes in streams_idx[] to 
      redraw the CamImgComponents

- streams_ids is an array of the streams IDs

- streams_img_urls is an array of the stream image blobs urls

- streams_names is an array of the streams names


The methods that will be called by the timer is set up in the contructor

    - get_active_streams() for gathering all available live streams and 
      IDs of those streams that are inactive

    - populate_streams_arrays() enters values for 
      streams_names[], streams_idx[] and streams_ids[]

    - get_streams_imgs() for getting the images every 5 secs

------------------------------------------------------------- */
