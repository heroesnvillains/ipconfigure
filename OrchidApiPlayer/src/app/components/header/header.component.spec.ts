import { ComponentFixture, TestBed } from '@angular/core/testing'
import { By } from '@angular/platform-browser'
import { HeaderComponent } from './header.component'

describe('HeaderComponent', () => {

    let component: HeaderComponent
    let fixture: ComponentFixture<HeaderComponent>

    beforeEach( async () => {

        TestBed.configureTestingModule({
            declarations: [ HeaderComponent ]
        })

        fixture = TestBed.createComponent(HeaderComponent)
        component = fixture.componentInstance
    })

    it('should be creatable', () => {
        
        expect(component).toBeTruthy()
    })

    it(`should have as title 'Orchid API Player'`, () => {

        const fixture = TestBed.createComponent(HeaderComponent)
        const component = fixture.componentInstance
        expect(component.title).toEqual('Orchid API Player')
    })

    it('should contain a div element', () => {

        const div_elem = fixture.debugElement.queryAll(By.css('div'))
        expect(div_elem.length == 1).toBeTruthy()
    })

})
