import { Component } from '@angular/core'

@Component({
    selector    : 'app-root',
    templateUrl : './app.component.html',
    styleUrls   : ['./app.component.css']
})

export class AppComponent {

    title = 'Orchid API Player'

}


/* ---------------------------------------------------------

AppComponent is the main container for application. 
It contains :

- the HeaderComponent that draws the application header

- the CamImgContainerComponent that displays the camera stream names and images in a grid

------------------------------------------------------------- */
