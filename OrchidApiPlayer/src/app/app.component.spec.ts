import { ComponentFixture, TestBed } from '@angular/core/testing'
import { By } from '@angular/platform-browser'
import { RouterTestingModule } from '@angular/router/testing'
import { AppComponent } from './app.component'
import { Component } from '@angular/core'

describe('AppComponent', () => {

    let component : AppComponent
    let fixture   : ComponentFixture<AppComponent>

    @Component({

        selector: 'app-header',
        template: '<div></div>'
    })
    class HeaderComponentMock{}

    @Component({

        selector: 'app-cam-img-container',
        template: '<div></div>'
    })
    class CamImgContainerComponentMock{}

    beforeEach(() => {

        TestBed.configureTestingModule({

            imports: [
                RouterTestingModule
            ],
            declarations: [
                AppComponent,
                HeaderComponentMock,
                CamImgContainerComponentMock
            ],
        })

        fixture   = TestBed.createComponent(AppComponent)
        component = fixture.componentInstance
    })

    it('should be creatable', () => {

        expect(component).toBeTruthy()
    })

    it(`should have as title 'Orchid API Player'`, () => {

        expect(component.title).toEqual('Orchid API Player');
    })

    it('should contain app-header element', () => {

        const elem = fixture.debugElement.query(By.css('app-header'))

        expect(elem.name).toEqual('app-header')
    })

    it('should contain app-cam-img-container element', () => {

        const elem = fixture.debugElement.query(By.css('app-cam-img-container'))

        expect(elem.name).toEqual('app-cam-img-container')
    })

});
