class PiEstimator {

    constructor(){}

    compute(n_iter){

        const pi = this.arctan_taylor_series(1.0, n_iter) * 4.0
        console.log(`\nEstimated Pi: ${pi} (over ${n_iter} iterations)`)
        this.error(pi)
    }

    arctan_taylor_series(x, n_iter){

        let i = 0
        let sign = -1.0
        let arctan = 0.0

        for (let i_iter = 0; i_iter < n_iter; i_iter++){

            i = 2*i_iter + 1
            sign *= -1
            arctan += sign * Math.pow(x, i)/i
        }

        return arctan
    }

    error(pi){
        console.log(`\nError: ${Math.abs(Math.PI - pi)}`)
    }
}

const pe = new PiEstimator()
pe.compute(20)
